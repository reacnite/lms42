======================================== Tokens ========================================
'if' <keyword>
'(' <delimiter>
'1' <number>
')' <delimiter>
'{' <delimiter>
'var' <keyword>
'z' <identifier>
'=' <operator>
'"A string"' <string>
';' <delimiter>
'if' <keyword>
'(' <delimiter>
'z' <identifier>
')' <delimiter>
'{' <delimiter>
'}' <delimiter>
'else' <keyword>
'{' <delimiter>
'var' <keyword>
'something' <identifier>
';' <delimiter>
'}' <delimiter>
'}' <delimiter>
'' <eof>

======================================== AST ========================================
tree.Program(
    block=tree.Block(
        statements=[
            tree.IfStatement(
                condition=tree.Literal(value=1),
                yes=tree.Block(
                    statements=[
                        tree.Declaration(
                            identifier='z',
                            expression=tree.Literal(value='A string')
                        ),
                        tree.IfStatement(
                            condition=tree.VariableReference(name='z'),
                            yes=tree.Block(statements=[]),
                            no=tree.Block(
                                statements=[
                                    tree.Declaration(
                                        identifier='something',
                                        expression=None
                                    )
                                ]
                            )
                        )
                    ]
                ),
                no=None
            )
        ]
    )
)

======================================== Running program ========================================

import sys
from dataclasses import dataclass
from typing import Union
import prettyprinter


# To be used to store the current values of runtime variables.
variables = {}


def runtime_error(message):
    """Print a message and exit."""
    print(message)
    sys.exit(1)


prettyprinter.install_extras(include=['dataclasses'])

def print_ast(ast):
    """Display an indented view of the AST."""
    prettyprinter.pprint(ast)


@dataclass
class Statement:
    pass


@dataclass
class Expression(Statement):
    pass


@dataclass
class Block(Statement):
    statements: list[Statement]
    def run(self):
        TODO()


@dataclass
class Program:
    block: Block
    def run(self):
        variables.clear()
        self.block.run()


@dataclass
class Declaration(Statement):
    identifier: str
    expression: Expression
    def run(self):
        TODO()


@dataclass
class IfStatement(Statement):
    condition: Expression
    yes: Statement
    no: Statement
    def run(self):
        TODO()


@dataclass
class WhileStatement(Statement):
    condition: Expression
    statement: Statement
    def run(self):
        while self.condition.run():
            self.statement.run()


@dataclass
class BinaryOperator(Expression):
    """Supports the following operator: = += -= *= /= + - * / == != >= <= > < %"""
    left: Expression
    operator: str
    right: Expression
    def run(self):
        TODO() # This one will be pretty long!


@dataclass
class Literal(Expression):
    value: Union[int,str]
    def run(self):
        return self.value


@dataclass
class VariableReference(Expression):
    name: str
    def run(self):
        if not self.name in variables:
            runtime_error(f"Variable '{self.name}' is not declared")
        return variables[self.name]


@dataclass
class FunctionCall(Expression):
    name: str
    arguments: list[Expression]
    def run(self):
        """Run the built-in functions. An actual programming language would probably allow
        definition of functions within the language itself."""
        args = [arg.run() for arg in self.arguments]
        TODO()
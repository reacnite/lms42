import unittest
from bit_reader import BitReader

class BitReaderTest(unittest.TestCase):

    def test_bit(self):
        br = BitReader(b'\x0f\xf0')
        for bit in range(4):
            self.assertEqual(br.get_bit(), 0, f"Bit {bit} in sequence 1")
        for bit in range(8):
            self.assertEqual(br.get_bit(), 1, f"Bit {bit} in sequence 2")
        for bit in range(4):
            self.assertEqual(br.get_bit(), 0, f"Bit {bit} in sequence 3")
        with self.assertRaises(IndexError):
            br.get_bit()

    def test_unsigned(self):
        br = BitReader(b'\x40\x00\x02\x12\x34\x56')
        self.assertEqual(br.get_number(8), 0x40)
        self.assertEqual(br.get_number(16), 0x0002)
        self.assertEqual(br.get_number(4), 0x1)
        self.assertEqual(br.get_number(8), 0x23)
        self.assertEqual(br.get_number(12), 0x456)
        with self.assertRaises(IndexError):
            br.get_number(1)

        br = BitReader(b'\x01\xff\xff\xff\xfe')
        self.assertEqual(br.get_number(7), 0)
        self.assertEqual(br.get_number(32), 0xffffffff)
        self.assertEqual(br.get_number(1), 0)
        with self.assertRaises(IndexError):
            br.get_number(1)

    def test_signed(self):
        br = BitReader(b'\x80\xff\x00\x7f')
        self.assertEqual(br.get_signed_number(8), -128)
        self.assertEqual(br.get_signed_number(8), -1)
        self.assertEqual(br.get_signed_number(8), 0)
        self.assertEqual(br.get_signed_number(8), 127)
        with self.assertRaises(IndexError):
            br.get_signed_number(1)

        br = BitReader(b'\x70\x8F')
        self.assertEqual(br.get_signed_number(4), 7)
        self.assertEqual(br.get_signed_number(4), 0)
        self.assertEqual(br.get_signed_number(4), -8)
        self.assertEqual(br.get_signed_number(4), -1)
        with self.assertRaises(IndexError):
            br.get_signed_number(1)


if __name__ == '__main__':
    unittest.main()

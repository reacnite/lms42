name: Roles & project management
description: What roles will you commonly find in a software company and how do they collaborate?
goals:
    roles: 1
    projectmanagement: 2
days: 2
avg_attempts: 1
allow_longer: true
public: users

assignment:
    Exam work: |
        <div class="notification is-warning">
            Please note that this assignment counts as part of the module exam. You *are* allowed to attempt this assignment as many times as is required to get to a passing grade though. (Which will hopefully be just once.)
        </div>

    Case study: |
        InsuWare BV is a (fictitious) software development agency located in Enschede. It specializes in the creation of custom software for the insurance industry.

        The company has three separate division, each of which can work for a different client. Each division consists of four front-end developers, three backend-developers, a UX designer, an insurance analyst, a software architect and a product owner.

        Divisions are dedicated to a single client for two weeks at a time (sprints), during which time they try to make themselves as useful for the client as they can. After each sprint, clients are free to book another sprint or to end the collaboration. InsuWare will therefore constantly have to prove its worth.

        Apart from the divisions, the company has a CEO, a CFO, an office manager, a marketeer, an HR officer and three account managers.

    Roles:
    -
        link: https://www.patrickbetdavid.com/roles-within-a-company/
        title: 10 different roles within a company
        info: In text as well as in video format.
    -
        link: https://www.indeed.com/career-advice/starting-new-job/business-roles
        title: 20 Essential Business Roles Within an Organization
    -
        link: https://www.youtube.com/watch?v=hvDTweQJWRg
        title: The Roles of a Software Development Team
        info: The roles described here are fairly typical for a software development agency, producing custom-built software for corporate clients. Companies developing just their own product (like Thuisbezorgd), can have different requirements.
    -
        link: https://dutchreview.com/expat/work/wages-netherlands-dutch-salaries/
        title: "Salaries in the Netherlands: the ultimate guide to Dutch wages"

    -
        text: |
            For each of the 13 *roles* mentioned in the case study...

            - Try to estimate the monthly gross salary. You may want to look at job boards to get a feel for the salaries paid for a role.
            - Describe (in one or two sentences each) in what way the role interacts with (other) software developers. Some information has been provided in the resources above, but you will likely have to search for more information yourself.
        type: rubric
        map:
            roles: 1
        scale: 10
        6: |
            Many descriptions are a bit hand wavy/unspecific but point in the right direction. Most of the important tasks are somewhat covered. Salaries are within a factor of 2 of what seems reasonable.
        9: |
            Each role is clearly described. All important tasks are covered. Salaries are convincing.


    Project management process:
    -
        link: https://www.youtube.com/watch?v=Y_A0E1ToC_I&t=20s
        title: Waterfall model definition and example
        info: The waterfall model used to be the most prominent software development strategy, until the rise of agile. It's still the best choice in some circumstances.

    -
        link: https://www.youtube.com/watch?v=rf8Gi2RLKWQ
        title: The Agile Manifesto - 4 Agile Values Explained
        info: A 2 minute introduction to the manifesto that changed the software development process.

    -
        link: https://www.youtube.com/watch?v=rIaz-l1Kf8w
        title: Scrum vs Kanban - What's the Difference?
        info: "This video explains the two most prominent flavours of Agile development: Scrum and Kanban."

    -
        link: https://www.youtube.com/watch?v=b8PHi1D193k
        title: The Spotify Scaling Agile Model
        info: This is a great example of how an agile way of working is being implemented in a large company. Of course we're not expecting to see anything this elaborate in this assignment, but this video *does* provide you with plenty of ideas and clearly shows the flexible attitude Spotify has towards the development process.

    -
        link: https://www.youtube.com/watch?v=Hwu438QSb_g
        title: Learn agile estimation in 10 minutes
        info: This video is pretty crucial in understanding what's going on at most software development companies.

    -
        link: https://www.youtube.com/watch?v=TxSzo3lwwWQ&t=19s
        title: Planning Poker | Story Point Estimation in Agile | Agile Estimation Techniques

    -
        link: https://www.youtube.com/watch?v=TlXy_i27N3w
        title: How to review someone else's code
        info: Why and how to do code reviews. You may not need to watch all of this for the benefit of the assignment, but your (future) coworkers will appreciate it if you do!

    -
        link: https://www.youtube.com/watch?v=iBad0aqUfus
        title: Why We Practice Pair Programming
        info: Pair programming explained in a minute.
        
    -
        type: rubric
        scale: 10
        6: Mostly coherent, but only about 60% of all important questions are answered.
        9: Is coherent and answers 90% of all important questions.
        map:
            projectmanagement: 1
        text: |
            Drawing inspiration from the project management techniques explain in the above resources, come up with your own (wildly different?!) process for converting customer wishes into awesome, reliable software products.

            Your description of the process should mention the roles played by (at least) the account managers, front-end developers, backend-developers, UX designers, insurance analysts, software architect, product owner and of course the customers.

            Things to consider: Which regular meeting are there, with whom, about what, and how often? How is the decision made what should be worked on (first)? How are time estimated part of that decision? How is quality assured? 

            **Don't** use project management jargon, but describe the process in terms regular people can understand. A good length for your description would be between 400 and 600 words. 

            Of course, what we're asking here is pretty much impossible: this should be the job of a full-blown CTO with years of experience, not an almost-junior developer! The goal is to get you thinking about how collaboration *could* work. Your solution doesn't need to work great in practice in order to get a good grade for it. It *should* however describe a complete and internally consistent process in simple terms.


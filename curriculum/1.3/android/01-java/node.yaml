name: Java
description: Learn the Java language.
goals:
    java: 1
days: 3
resources:
    Polyglot:
        - |
            This module you'll take your first step towards become a polyglot programmer, a programmer that can use many different programming languages. You'll be learning the Java language!

            Why would you want to learn more than one programming language? Can't you just do everything with Python? Well, you can, in theory. All programming logic that can be expressed in one programming language, can also be expressed in Python, or another general purpose language. But there's more to it than that...

        -
            link: https://www.youtube.com/watch?v=GI_V3yzVDtA
            title: Different Types of Programming Languages
            info: This video discusses five fundamental differences between programming languages. A program can be 1) compiled or interpreted, 2) strongly or weakly and statically or dynamically typed, 3) object orientated and/or functional, 4) garbage collected (or not), and 5) built for a specific type of concurrency (or not).
        
        -
            link: https://www.youtube.com/watch?v=hx4NNTYb85c
            title: 10 Different Programming Languages and Their Uses
            info: This video discusses common programming languages, what makes them special, and for what types of programs they are typically used. This may help you get some idea one why there are some many different languages, and why it's worth it to learn more than one.

    Why Java?:

        - |
            So why Java? It is a compiled, strongly and statically typed, object-oriented garbage collected language. Java (like Python) has a large ecosystem of freely available libraries Java. Java programs run on the Java Virtual Machine (JVM). The JVM is just a program that must be installed on any device in order to run Java/Kotlin programs.

            Java and Kotlin are the only two programming languages that Google officially supports for developing Android applications. While Kotlin is a more modern language, and tends to produces programs that are shorter and more elegant, we're choosing to learn Java in this module, as it is simpler. If you continue to develop Android applications after this module, we highly recommend diving into Kotlin though!

        -
            link: https://www.javatpoint.com/features-of-java
            title: Java-T-point - Features of Java
            info: What makes Java a good programming language, and why would you want to use it?


    Hello world: |
        For programming in Java we're going to be using the IntelliJ IDE instead of Visual Studio Code. Although it is possible to develop Java from within Visual Studio Code, Java support in IntelliJ is much more advanced. 

        Also, when we'll start working on Android apps, we'll be using *Android Studio*, which is a Google-modified version of IntelliJ. So we'll need to learn to use IntelliJ anyhow!

        Using *Add/Remove Software* install `intellij-idea-community-edition` and `jdk-openjdk`. It'll ask you if you want to install `lldb` as well - you don't need it. (This may take a while. You can dive into the *Learning Java* section in the meantime.)

        When it's been installed:

        - Open IntelliJ.
        - Create a new project. You'll see a dialog for project settings:
            - Pick an appropriate name (`hello`?).
            - Language: Java.
            - Build system: IntelliJ.
            - JDK: Select the one you just installed.
            - Leave *Add sample code* on.
            - Press *Create*.
        - IntelliJ will download and build all required tools and *index* the project. Give it some time...
        - Open the `main.java` file. It contains a `main` method that will print *Hello world*.
        - Run the program by tapping the green triangle on the left of the `main` function signature line. This should compile the application (taking some time and producing some output), and the run it. 

    Learning Java:

        - |
            You'll notice that learning a second programming language is a lot easier than learning the first one. Therefore, we won't be holding your hand throughout the process, but will just allow you to explore and learn by yourself!

            Below are two resources you may want to use (but there are many others, if these are not too your taste). We'd recommend you spend some hours on (one of) them, **while immediately experimenting** with the things you learn in IntelliJ. After you feel you've learned enough, just dive into the assignments. When you notice you're missing some knowledge, go back to the tutorials (or use Google, of course).

        -
            link: http://anh.cs.luc.edu/170/html/Java4Python.html
            title: Java for Python Programmers
            info: We recommend that you read/scan this relatively short e-book, up until (not including) the "Replacing Python input" section.

        -
            link: https://www.javatpoint.com/java-tutorial
            title: Java-T-point
            info: This tutorial can be used as a reference, or as a second explanation in cases where "Java for Python Programmers" doesn't make sense to you.

        - |
            And really, don't forget to experiment. If you don't, it's unlikely the things you learn will stick.

assignment:
    - Assignment: |
        We'll be creating a state-of-the-art 1960s game, a classic text-based dungeon crawler! But of course, we'll be using a(slightly) more modern language: Java!

        A dungeon crawler is an adventure game in which the players discovers a large dungeon containing all sorts of monsters and treasures. Everything is described using just text. So in some way, it's more like an interactive book than a computer game.

    - Example game play: |
        <pre style="max-height: 66vh; overflow: auto; white-space: pre-wrap;">
        You have entered the dungeon. The door closes and silently vanishes behind you. That was to be expected. You notice that it's rather dark.
        You step into a remarkably uninteresting empty room.

        Health: 30/30
        Where would you like to go?
        s) go south
        <em>s</em>
        You boldly go south.
        You step into a remarkably uninteresting empty room.

        Health: 30/30
        Where would you like to go?
        n) go north
        e) go east
        <em>w</em>
        You boldly go west.
        You walk head-first into a wall. Hard. Don't laugh, it's pretty dark in here.
        You sneakily retreat to the previous room.

        Health: 30/30
        Where would you like to go?
        n) go north
        e) go east
        <em>e</em>
        You boldly go east.
        You step into a remarkably uninteresting empty room.

        Health: 30/30
        Where would you like to go?
        e) go east
        s) go south
        w) go west
        <em>e</em>
        You boldly go east.
        You walk into a room that conveniently contains a health pack, just lying there. Weird.
        Would you like to use it?
        y) yes
        n) no
        <em>y</em>
        You feel better than you ever felt before. This is great. You want more. More. More! MORE!!

        Health: 40/40
        Where would you like to go?
        w) go west
        <em>w</em>
        You boldly go west.
        You step into a remarkably uninteresting empty room.

        Health: 40/40
        Where would you like to go?
        e) go east
        s) go south
        w) go west
        <em>s</em>
        You boldly go south.
        You step into a remarkably uninteresting empty room.

        Health: 40/40
        Where would you like to go?
        n) go north
        s) go south
        <em>s</em>
        You boldly go south.
        You encounter a dungeon cat!
        Dungeon cats are not actually cats. They're way too big for that. And they look more like giant hamsters. They do live in the dungeons though.
        The dungeon cat is a multiplier 1 monster. The damage it does is determined by a single dice, multiplied by 1.

        Monster health: 10/10
        What would you like to do?
        a) attack
        r) retreat
        <em>a</em>
        You're rolling your dice...  2
        The dungeon cat prepares to strikes back.
        The dungeon cat throws... 4
        Health: 36/40

        Monster health: 8/10
        What would you like to do?
        a) attack
        r) retreat
        <em>a</em>
        You're rolling your dice...  1
        The dungeon cat prepares to strikes back.
        The dungeon cat throws... 5
        Health: 31/40

        Monster health: 7/10
        What would you like to do?
        a) attack
        r) retreat
        <em>a</em>
        You're rolling your dice...  5
        The dungeon cat prepares to strikes back.
        The dungeon cat throws... 1
        Health: 30/40

        Monster health: 2/10
        What would you like to do?
        a) attack
        r) retreat
        <em>a</em>
        You're rolling your dice...  5
        You killed it until it was dead! Well done.

        Health: 30/40
        Where would you like to go?
        n) go north
        e) go east
        <em>e</em>
        You boldly go east.
        You step into a remarkably uninteresting empty room.
        </pre>

    - Class diagram: |
        Below is a class diagram for what should be the final version of your program. Click it to enlarge. You can use it throughout the assignment to draw 'inspiration'. You can consider the provided method names as hints - you are not required to match them exactly.

        ```plantuml

        package explorables {
            interface Explorable {
                {abstract} +explore(Hero)
            }

            abstract class Monster {
                -health: int
                +Monster()
                +explore(Hero)
                +throwDice(count: int): int
                {abstract} +getName(): String
                {abstract} +getDescription(): String
                {abstract} +getInitialHealth(): int
                {abstract} +getDamage(): int
                {abstract} +getAttackDescription(): String
            }

            Monster --|> Explorable

            abstract class ConstantMonster {
                +getAttackDescription(): String
            }

            abstract class ExponentialMonster {
                +getAttackDescription(): String
                +getDamage(): int
                {abstract} +getExponent(): int
            }

            abstract class MultiplierMonster {
                +getAttackDescription(): String
                +getDamage(): int
                {abstract} +getMultiplier(): int
            }

            ConstantMonster --|> Monster
            MultiplierMonster --|> Monster
            ExponentialMonster --|> Monster

            class OrcWarrior {
                +getName(): String
                +getDescription(): String
                +getInitialHealth(): int
                +getDamage(): int
            }
            OrcWarrior --|> ConstantMonster

            class VampireDragon {
                +getName(): String
                +getDescription(): String
                +getInitialHealth(): int
                +getDamage(): int
            }
            VampireDragon --|> ConstantMonster

            class CursedDragon {
                +getName(): String
                +getDescription(): String
                +getInitialHealth(): int
                +getExponent(): int            
            }
            CursedDragon --|> ExponentialMonster

            class MagmaElemental {
                +getName(): String
                +getDescription(): String
                +getInitialHealth(): int
                +getExponent(): int            
            }
            MagmaElemental --|> ExponentialMonster

            class DungeonCat {
                +getName(): String
                +getDescription(): String
                +getInitialHealth(): int
                +getMultiplier(): int            
            }
            DungeonCat --|> MultiplierMonster

            class CommonHouseDragon {
                +getName(): String
                +getDescription(): String
                +getInitialHealth(): int
                +getMultiplier(): int            
            }
            CommonHouseDragon --|> MultiplierMonster

            class HealthPack {
                -used: bool
                +explore(Hero)
            }
            HealthPack --|> Explorable

            class Exit {
                +explore(Hero)
            }
            Exit --|> Explorable

            class Wall {
                +explore(Hero)
            }
            Wall --|> Explorable

            class Empty {
                +explore(Hero)
            }
            Empty --|> Explorable

            class Map {
                +getSquare(Position): Explorable
                +isWall(Position): bool
            }
            Explorable "width*height" <--o Map 

            class Game {
                +main()
            }
            
            class Hero {
                -health: int
                -maxHealth: int
                -winner: bool
                +playTurn(Map)
                +receiveDamage(int)
                +retreat()
                +throwDice(): int
                +upgradeHealth()
                +declareWinner()
                +isWinner(): bool
                +isDead(): bool
            }

            class Position {
                -x: int
                -y: int
                +getNeighbour(direction: Char): Position
                {static} +DIRECTIONS: HashMap<direction: Char, name: String>
            }
            note top of Position
                DIRECTIONS contains {'n': "north", 'e': "east", 's': "south", 'w': "west"}
            endnote

            Position <--o Hero : current
            Position "0..*" <--o Hero : previous

            Map <--o Game
            Hero <--o Game
        }

        ```

    -
        ^merge: feature
        title: A map of walls
        weight: 2
        text: |
            Create the first part of the above game, allowing the user to navigate a dungeon consisting of just empty spaces and walls.

            For that, you'll need to create the `Explorable` interface, and create the `Wall` and `Empty` classes that implement it.

            Next, create a `Map` class that has a two-dimensional array of `Explorable` objects. The constructor should instantiate all the walls and empty spaces based on the `mapString` provided below. For now, everything except `#` may be considered empty space.

            ```
            static final String[] mapString = new String[] {
                "##################################",
                "# ##H   ########          H#######",
                "#  H###          #####3###########",
                "## #2  #         #   #         HH#",
                "##1  # #######6### # #########2###",
                "### ## 1 #  3      # ######### ###",
                "#HH4   ############# ######### ###",
                "### ########                     #",
                "#    1H##### ####### ######### ###",
                "#  4  ###### ####### ######### # #",
                "#1#    ##### #### ## #         # #",
                "#H2    6         #############   #",
                "# ########## ###              ####",
                "#  3 4 # 2 # ################    #",
                "########H    #    5   6H #######H#",
                "# 4 5     #### ######### #########",
                "# 4 5      4HH #########12345   *#",
                "##################################"
            };
            ```

            Finally, create a `Hero` class that has a `Position`, starting at `(1,1)`, the upper left corner. The main `Game` class should repeatedly show the user which directions can be taken, accept input, and move the hero in that direction. Next it should `explore` that room, the behavior of which is determined by the square of the map (the room) the hero enters. In case the user runs into a wall, the Wall's `explore` method should call `retreat` on the `Hero` which should cause the hero to move to her previous position.

            On every turn, all available information about the current room and hero health (starting at 30 out of a maximum of 30) should be shown. 

    -
        ^merge: feature
        title: Power-ups
        text: |
            Let's add two additional types of rooms, the `HealthPack` and the `Exit`.

            - A `HealthPack` should ask the player if she wants to consume it now. If she does, it will either:
              - Increase the hero's health to its maximum, if it wasn't already, **or**
              - Increase the hero's maximum health by 10 (without changing the current health level).
              When the health pack has been consumed, it cannot be used again when entering the room a second time. Health packs should be created in the map for all the squares that have an `H` in the `mapString`. 
            - Reaching the `Exit`, `*` in `mapString`, should cause the hero to be declared victorious and the game to exit. Also, it should output the complete list of directions taken by the player. For example: `It appears that the following is a heroes path: s s w e s e.....`. Use an `ArrayList` to keep track of this route.

    -
        ^merge: feature
        title: explorables package
        text: |
            As we're creating quite a few `Explorable` classes and that may lead to a confusing source tree, create an `explorables` Java package to move them all into that package. You may need to change some *access modifiers* to expose all required functionality outside of the package.

    -
        ^merge: feature
        title: Monsters
        weight: 2
        text: |
            Now let's add some monsters! Monsters are fixed in place, and will engage in a fight when our hero runs into them.

            There are 6 different types of monsters (indicated 1 through 6), but they only have 3 different types of behaviors. These 3 behaviors should be implemented as classes (`MultiplierMonster`, `ExponentialMonster`, `ConstantMonster`) all inheriting from a `Monster` class.

            Each of the 6 monster types should be implemented as classes, inheriting from the appropriate behavior classes.

            | id | Name | Description | Health | Behavior |
            | -- | -- | -- | -- | -- |
            | 1| dungeon cat | Dungeon cats are not actually cats. They're way too big for that. And they look more like giant hamsters. They do live in the dungeons though. | 10 | multiplier 1 |
            | 2 | common house dragon | Whoever invented that name probably never met one. | 25 | multiplier 3 |
            | 3 | cursed dragon | Fighting a cursed dragon can be the easiest thing you do today. Easier than brushing your teeth. Or it can be the hardest (and last) thing you'll ever do. | 15 | exponential 2 |
            | 4 | orc warrior | These nimble but incredibly skilled fighters have basically invented war. | 30 | constant 20 |
            | 5 | vampire dragon | Exactly like two strong warhorses. But flying. And breathing fire. | 45 | constant 30 |
            | 6 | magma elemental | Unknown. We're not even sure they exist. | 50 | exponential 3 | 

            Here is how the different behaviors work:
            
            | Behavior | Description |
            | -- | -- |
            | multiplier | The damage it does is determined by a single dice (1-6), multiplied by *the multiplier value for this monster type*. |
            | exponential | The damage it does is determined by a single dice (1-6), to the power of *the exponent value for this monster type*. |
            | constant | It always does the exact same damage, depending on the *damage value for this monster type*. |

            When entering a room that contains a monster:
            
            1. When the monster has been killed earlier, a message is shown that explains that fact.
            2. Otherwise, the user gets the choice to either retreat or attack.
            3. Retreat means the hero goes back to the previous square, and the turn ends.
            4. When attacking, the user rolls two dice and deals that amount of damage to the monster. In return, the monster deals the damage as described above.
            5. When the player dies, the *game* ends with a sad messages.
            6. When the monster dies, the *turn* ends with a happy message.
            7. In case nobody has died yet, we go back to step 2.
    

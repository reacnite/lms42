name: Internship orientation
description: Start looking for suitable internship companies.
days: 2
allow_longer: true
avg_attempts: 1
startable: false
type: overhead
public: users
grading: false
assignment: 
- |
    After (almost) completing period 2.1, you're expected to start your first internship!

    For that to happen, you need to start reaching out to companies well in advance. Now, at the start of your second year, is a good time for that. 
    
    For efficiency, we recommend that you try to arrange both internships in parallel. In order to find two suitable internships, a good number of companies to approach initially is four. That gives you options in case you don't like a company, or it is already saturated with interns.

    You will need to find a companies/institutions that do professional software development, that are willing to let you work as a (junior) member of the software development team for 12 weeks, 4 days per week. The development teams that you'll be part of should consist of at least three members that hold a relevant hbo+ degree. One of them should be willing to mentor you, as your internship supervisor.

    In order to keep the overhead for matching up with companies low for all involved parties, we recommend you take the following steps:

    - <a target="_blank" href="/internships" role="button" class="outline side">Internship database</a>The *Internship database* provides a list of companies that are -to the best of our knowledge- willing to accept interns and good employers, providing a professional and friendly environment. You can use it -or any other means- to find companies that you like. Aim to do your internships at two companies that are very different. You may be able to talk to (former) fellow students to hear about their experiences with the company. Also, Saxion HBO-ICT organizes a *business market* twice a year (around October and April), that allows students to meet talk to developers/recruiters from tens of companies.
    - <a target="_blank" href="AdSD-stage.pdf" role="button" class="outline side">Fact sheet for companies</a>Approach the team's contact person by mail or phone, providing a little background about yourself, why you'd like to intern at that company, and over what period you'd like to do that. You can attach the *Fact sheet for companies* document (Dutch-only at the moment) to your emails. Propose to visit in person if the contact person thinks there may be a place for you.
    - Your visit to the company will be like a job interview, but generally a bit less involved. You'll want to make a good impression on the person (or people) sitting across the table. But you'll also want to ask questions of your own, to make sure the company can offer what you're looking for. It is generally wise to prepare a list of questions in advance.
    - <a target="_blank" href="Internship Agreement AdSD.pdf" role="button" class="outline side">Standard agreement</a>In case you and company are ready to commit to an internship together (which may happen by the end of the meeting, or either party may ask to get back on that in a few days), the next step is to sign an *internship agreement*. We prefer that the *Standard agreement* is used. If the company wants to use its own agreement, you will need to ask your Saxion graduation supervisor to review it before signing. In order to speed things up, it may be a good idea to bring two copies of the (empty) standard agreement along during your visit to the company, in the off chance you may be able to sign it by the end of the meeting.

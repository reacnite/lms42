# Domain model

```plantuml
' Configuration stuff (you should not change this)
' hide the spot
hide circle
' avoid problems with angled crows feet
skinparam linetype ortho


' Add the entities here
entity Order {
  *date
}

entity Product {
  *price
  *name
  description
}

entity Category {
  *name
}

' Add notes here
note left of Order
  This is a multi line note:
  With <i>italcs</i> and <b>bold</b> markup
end note


' Add the relationships here
Order "contains" ||--o{ Product

Product }o--o{ Category

```
from ..app import db, app
from .user import User
import datetime
import sqlalchemy

class ApiToken(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    token = db.Column(db.String, nullable=False)

    creation_time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship(User, foreign_keys="ApiToken.user_id")

    host = db.Column(db.String, nullable=False)

    __table_args__ = (
        sqlalchemy.UniqueConstraint('user_id', 'host', name='api_token_user_host'),
    )

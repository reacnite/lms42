from lms42.models.attempt import Attempt
from lms42.working_days import offset_hours, get_working_hours_delta, DAY_HOURS
from datetime import datetime, date

def test_offset_hours():
    # Dates are in summer time, meaning working days last from 6:30 until 15:00

    # Start early, skip the weekend
    assert offset_hours(datetime(2023,6,2, 6,00), 10.5) == datetime(2023,6,5, 8,30)

    # Also skip the Monday
    assert offset_hours(datetime(2023,6,2, 6,00), 10.5, {date(2023,6,5)}) == datetime(2023,6,6, 8,30)

    # Now skip the initial Friday
    assert offset_hours(datetime(2023,6,2, 6,00), 10.5, {date(2023,6,2)}) == datetime(2023,6,6, 8,30)

    # Make sure a day last for DAY_HOURS
    assert offset_hours(datetime(2023,6,5, 8,00), DAY_HOURS) == datetime(2023,6,6, 8,00)

    # Start really early and end by the end of the first day
    assert offset_hours(datetime(2023,6,5, 3,0), DAY_HOURS) == datetime(2023,6,5, 15,00)

    # Start at night, for 1.5 day.
    assert offset_hours(datetime(2023,6,12, 19,54), DAY_HOURS*1.5) == datetime(2023,6,14, 10,45)


def test_get_working_hours_delta():
    # Dates are in summer time, meaning working days last from 6:30 until 15:00

    fri_at_7 = datetime(2023,6,2, 7,00)

    assert get_working_hours_delta(fri_at_7.replace(hour=6), fri_at_7) == 0.5
    assert get_working_hours_delta(fri_at_7, fri_at_7.replace(hour=8)) == 1
    assert get_working_hours_delta(fri_at_7.replace(hour=4), fri_at_7.replace(hour=20)) == DAY_HOURS
    assert get_working_hours_delta(fri_at_7.replace(hour=4), fri_at_7.replace(day=5, hour=20)) == 2*DAY_HOURS
    assert get_working_hours_delta(fri_at_7, fri_at_7.replace(day=6)) == 2 * DAY_HOURS
    assert get_working_hours_delta(fri_at_7, fri_at_7.replace(day=6), {date(2023,6,5)}) == DAY_HOURS
    assert get_working_hours_delta(fri_at_7.replace(hour=12), fri_at_7.replace(hour=11)) == 0
    assert get_working_hours_delta(fri_at_7.replace(hour=15,minute=35), fri_at_7.replace(hour=16,minute=35)) == 0

    assert get_working_hours_delta(datetime(2023,9,20,17,59,53), datetime(2023,9,21,11,30)) == 5

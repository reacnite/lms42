def test_start_pair_programming(client, db_session):
    client.login("student1")
    start_and_submit_attempt(client, "html")

    client.login("student2")
    start_and_submit_attempt(client, "html")
    start_and_submit_attempt(client, "css")

    client.login("teacher1")
    grade_attempt(client, 'html', 'student1')
    grade_attempt(client, 'html', 'student2')

    client.login("student4")
    client.open(f"/curriculum/css")
    client.find("a", text="pair programming").require(1)
    client.find("a.pair_option", href="/people/student1").require(1)
    client.find("a.pair_option", href="/people/student2").require(0)
    client.find("a.pair_option", href="/people/student3").require(0)
    client.find("a.pair_option", href="/people/student4").require(0)


def start_and_submit_attempt(client, node_id):
    client.open(f"/curriculum/{node_id}")
    client.find("input", value='Start attempt anyway').submit()
    client.find("input", value='Submit attempt').submit(finished="yes")
    client.find("input", value="I'm sure").submit()


def grade_attempt(client, node_id, student_short_name, action="passed"):
    client.open(f'/curriculum/{node_id}?student={student_short_name}')
    form_data = {
        "formative_action": action
    }
    for element in client.find("input", type="radio", value="3"):
        form_data[element.get('name')] = "3" if action=="passed" else "1"
    for element in client.find("input", type="radio", value="yes"):
        form_data[element.get('name')] = "yes" if action=="passed" else "no"
    
    client.find("input", value="Publish", limit=1).submit(**form_data)
    client.find("article.notification", in_text = " passed with a ").require(1)
